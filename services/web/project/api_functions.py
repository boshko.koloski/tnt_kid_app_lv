import pandas as pd
from .preprocessing import batchify_docs, Corpus
from .keyword_extraction_main import predict
from .tf_idf_solve import predict_tf_idf

"""
def extract_keywords(text, model, dictionary, sp, stemmer, args):
    all_docs = [[1, text]]
    df_test = pd.DataFrame(all_docs)
    df_test.columns = ["id", "text"]
    corpus = Corpus(df_test, dictionary, sp, args)
    test_data = batchify_docs(corpus.test, 1)
    model.eval()
    predictions = predict(test_data, model, sp, corpus, args)
    predictions_lemmas = []
    predictions_tfidf = predict_tf_idf(text, lang="lv_nat")
    for kw in predictions:
        if len(kw.split()) == 1:
            lemma_kw = stemmer(kw)
        else:
            lemma_kw = kw
        predictions_lemmas.append(lemma_kw)
    
    predictions_tfidf_out = set()
    for kw in predictions_tfidf:
        if len(kw.split()) == 1:
            lemma_kw = stemmer(kw)
        else:
            lemma_kw = kw
        predictions_tfidf_out.add(lemma_kw)
    
    predictions_tfidf_out = list(predictions_tfidf_out)
    return predictions_lemmas[:args['kw_cut']], predictions_tfidf_out[:args['kw_cut']]
"""
    

def extract_keywords(text, model, dictionary, sp, tagset, stemmer, args):
    all_docs = [[1, text]]
    df_test = pd.DataFrame(all_docs)
    df_test.columns = ["id", "text"]
    corpus = Corpus(df_test, dictionary, sp, args)
    test_data = batchify_docs(corpus.test, 1)
    model.eval()
    predictions = predict(test_data, model, sp, corpus, args)
    predictions_lemmas = []
    for kw in predictions:
        if len(kw.split()) == 1:
            lemma_kw = stemmer(kw)
        else:
            lemma_kw = kw
        predictions_lemmas.append(lemma_kw)
        
    not_present = []
    present = []
    for idx, lemma in enumerate(predictions_lemmas):
        kw = predictions[idx]
        if lemma not in tagset and kw not in tagset:
            not_present.append(kw)
        else:
            if kw in tagset:
                kw = tagset[kw]
            if lemma in tagset:
                kw = tagset[lemma]
            present.append(kw)
            
    predictions_tf_idf = predict_tf_idf(text, lang="lv_nat")
    for kw in predictions_tf_idf:
        lemma_kw = " ".join([stemmer(word) for word in kw.split()])
        if lemma_kw not in tagset and kw not in tagset:
            if kw not in not_present and lemma_kw not in not_present:
                not_present.append(kw)
        else:
            if lemma_kw not in predictions_lemmas and kw not in predictions_lemmas:
                if kw in tagset:
                    kw = tagset[kw]
                if lemma_kw in tagset:
                    kw = tagset[lemma_kw]
                if kw not in present:
                    present.append(kw)
    return present[:args['kw_cut']], not_present[:args['kw_cut']]


