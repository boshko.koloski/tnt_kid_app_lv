import os
import json

from flask import (
    Flask,
    jsonify,
    send_from_directory,
    request,
    redirect,
    url_for
)

from flask_restx import Api, Resource, fields, abort, reqparse
import werkzeug
werkzeug.cached_property = werkzeug.utils.cached_property
from werkzeug.utils import secure_filename
from werkzeug.middleware.proxy_fix import ProxyFix

from . import api_functions
from . import keyword_extraction_main as kw


import LatvianStemmer

lemmatizer = LatvianStemmer.stem


app = Flask(__name__)
app.wsgi_app = ProxyFix(app.wsgi_app)
api = Api(app, version='1.0',
          title='API services',
          description='TNT-KID Latvian keyword extractor REST API')
ns = api.namespace('rest_api', description='REST services API')

args = {
    'max_length': 256,
    'cuda': False,
    'kw_cut': 10,
    'stemmer': lemmatizer,
    'split_docs': True,   
    'bpe': True, 
}

kw_model_path = "model_latvian_model12_folder_latvian_loss_1.4285130697847686_epoch_10.pt"
kw_dictionary_path = "latvian_model12_noadaptive_lm_bpe_nopos_rnn_nocrf.ptb"

kw_model = kw.loadModel(os.path.join("project", "trained_classification_models", kw_model_path), args['cuda'])# 'project/trained_classification_models/model_5_lm+bpe+rnn_croatian_big_folder_croatian_loss_2.4768645693382894_epoch_8.pt', args['cuda'])
kw_dictionary = kw.loadDict(os.path.join("project", "dictionaries", kw_dictionary_path)) #'project/dictionaries/dict_5_lm+bpe+rnn_croatian_big_adaptive_lm_bpe_nopos_rnn_nocrf.ptb')
tagset_lv = kw.get_tagset('project/tf_idf_files/latvian_keywords.csv')

kw_sp = kw.spm.SentencePieceProcessor()
kw_sp.Load(os.path.join("project","bpe", "latvian.model"))


# input and output definitions
kw_extractor_input = api.model('KeywordExtractorInput', {
    'text': fields.String(required=True, description='Title + lead + body of the article'),
})

kw_extractor_output = api.model('KeywordExtractorOutput', {
    'keywords_in_tagset': fields.List(fields.String, description='Extracted keywords'),
    'keywords_new': fields.List(fields.String, description='Extracted tfidf keywords')  
})


@ns.route('/extract_keywords/')
class KeywordExtractor(Resource):
    @ns.doc('Extracts keywords from news article')
    @ns.expect(kw_extractor_input, validate=True)
    @ns.marshal_with(kw_extractor_output)
    def post(self):
        kw_lem, kw_tfidf = api_functions.extract_keywords(api.payload['text'], kw_model, kw_dictionary, kw_sp, tagset_lv, lemmatizer, args)
        return {"keywords_in_tagset": kw_lem, "keywords_new" : kw_tfidf}



@ns.route('/health/')
class Health(Resource):
    @ns.response(200, "successfully fetched health details")
    def get(self):
        return {"status": "running", "message": "Health check successful"}, 200, {}
